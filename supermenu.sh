#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
    red=`tput setaf 1`;
    green=`tput setaf 2`;
    blue=`tput setaf 4`;
    bg_blue=`tput setab 4`;
    reset=`tput sgr0`;
    bold=`tput setaf bold`;
#------------------------------------------------------
# VARIABLES GLOBALES
#------------------------------------------------------

proyectoActual="/home/florencia/git-version-1.9.1/sor-tp.git";
proyectos="/home/Florencia/Documents/repo_GitLab/repos.txt";
#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";

    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";

    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver estado del proyecto";
    echo -e "\t\t\t b.  Saber si un programa esta instalado";
    echo -e "\t\t\t c.  Buscar en ese archivo";
    echo -e "\t\t\t d.  Guardar palabras repetidas de un archivo en otro";
    echo -e "\t\t\t e.  Ver estados del proceso 'estadosDeProceso'";
    echo -e "\t\t\t f.  Crear Acceso Directo";
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
    echo $1;
    while true; do
        echo "desea ejecutar? (s/n)";
            read respuesta;
            case $respuesta in
                [Nn]* ) break;;
                   [Ss]* ) eval $1
                break;;
                * ) echo "Por favor tipear S/s ó N/n.";;
            esac
    done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
        imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
        decidir "cd $proyectoActual; git status"; 
}

b_funcion () {
   imprimir_encabezado "\tOpción b";
   echo "ingrese nombre del programa a encontrar";
   read programa;
respuesta="";
   if [ "$(dpkg --get-selections | grep -w $programa)" = "$respuesta" ] ; then 
    echo "no esta instalado";
    # sudo aptitude -y isntall ${programa};
else
    echo "${programa}esta instalado"; 
fi
    #completar
}

c_funcion () {
	imprimir_encabezado "\tOpción c";

	echo "path:";
	read ruta;
	echo "extension:";
	read extension;
	echo "nombre de archivo:";
	read nombre
respuesta="";
	      # decidir "find $ruta -name "*.$extension" | grep –i $nombre";
	if [ "$(find $ruta -name "*.$extension" | grep -i $nombre)" = "$respuesta" ] ; then 
	echo "No se encuentra";	 

	else 
	echo "Los archivos $nombre: "
        find $ruta -name "*.$extension" | grep -i $nombre; #busca en una ruta todos los que tenga una extension y filtra lo que tienen tal nombre sin importar la mayuscula o minuscula
fi
}

d_funcion () {
    imprimir_encabezado "\tOpción d";

    echo "directorio del archivo de texto:";
    read directoriotexto; 
    echo "archivo:";
    read archivo;
    echo "palabra a buscar:";
    read palabra;
cadena="";
if [ "$(find $directoriotexto/$archivo)" = "$cadena" ];then  #busca un directorio
echo "No existe el archivo";				
else decidir "$(cat  $directoriotexto/$archivo | grep -n "$palabra">>$directoriotexto/salida.out)"; #concatena en un archivo todos los numeros de lineas donde aparece en el archivo la palabra ingresada.
fi
}


e_funcion () {
    imprimir_encabezado "\tOpción e";

    echo "proceso:";
    read proceso;
cadena="";
 gnome-terminal -e "bash -c \"./$proceso; exec bash;\""
  
  decidir "top -d 1000 -n 7 | grep $proceso"; 


pid=$(pgrep $proceso)

kill $pid
#pid=$(pgrep gnome-terminal-)
#kill $pid 
  
    #completar
}


f_funcion()
{

 pathactual=$(dirname $(readlink -f $0))

    echo "Nombre del Acceso Directo:";
    read nombreprograma;


    echo "Comentario:";
    read comentario


touch $pathactual/$nombreprograma.desktop
texto="[Desktop Entry]"


echo $texto >> $pathactual/$nombreprograma.desktop

texto="Version=1.0"

echo $texto >> $pathactual/$nombreprograma.desktop

texto="Name=$nombreprograma"
echo $texto >> $pathactual/$nombreprograma.desktop


texto="Comment=Comentario_que_quieras_hacer_sobre_el_programa";

echo $texto >> $pathactual/$nombreprograma.desktop


texto="Exec=$pathactual/supermenu.sh";

echo $texto >> $pathactual/$nombreprograma.desktop

texto="Icon=$pathactual/superman";
echo $texto >> $pathactual/$nombreprograma.desktop

texto="Encoding=UTF-8";

echo $texto >> $pathactual/$nombreprograma.desktop

texto="Terminal=true";
echo $texto >> $pathactual/$nombreprograma.desktop

texto="Type=Application";

echo $texto >> $pathactual/$nombreprograma.desktop

texto="Categories=Application;network";

chmod 777 $pathactual/$nombreprograma.desktop 

sudo mv  $pathactual/$nombreprograma.desktop /home/$USER/Desktop/$nombreprograma.desktop
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;

    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;

        f|F) f_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done