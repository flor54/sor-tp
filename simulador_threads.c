#include <stdlib.h>    
#include <unistd.h>         // para castear de puntero a entero
#include <string.h>
#include <time.h>
#include<stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
///////////////////////////////////////////////////////////////////////////
// Info de grupos
///////////////////////////////////////////////////////////////////////////
#define CANTIDAD_GRUPOS 8
char* grupos_labels[] = {"A", "B", "C", "D", "E", "F", "G", "H"};
sem_t miSemaforo,miSemaforo2,miSemaforo3,miSemaforo4,miSemaforo5;
pthread_mutex_t mi_mutex;
//cada grupo tiene
//4 filas, una por cada pais
//y 8 columnas:          
//columna 1 = Pts(Puntos)     
//columna 2 = PJ(Partidos Jugados)
//columna 3 = PG(Partidos Ganados)
//columna 4 = PE(Partidos Empatados)
//columna 5 = PP(Partidos Perdidos)
//columna 6 = GF(Goles a Favor)
//columna 7 = GC(Goles en contra)
//columna 8 = Dif(Diferencia de Gol)


/*
struct parametros {

 int equipo_1;
int equipo_2;
int instancia;
char* grupo_data[];
int grupo_puntos[][8];
int grupo_favoritos[];
};

*/
struct datpartido{

char *nombre_equipo_1;
char *nombre_equipo_2; 
int index_partido;
char**data_winners;

};

char* grupo_A[] = {"Rusia", "Arabia Saudita", "Egipto", "Uruguay"};
int grupo_A_favoritos[] = {1, 0, 0, 1};
int grupo_A_puntos[4][8] = { { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} };


char* grupo_B[] = {"Portugal", "España", "Marruecos", "Irán"};
int grupo_B_favoritos[] = {1, 1, 0, 0};
int grupo_B_puntos[4][8] = { { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} };


char* grupo_C[] = {"Francia", "Australia", "Peru", "Dinamarca"};
int grupo_C_favoritos[] = {1, 0, 0, 0};
int grupo_C_puntos[4][8] = { { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} };


char* grupo_D[] = {"Argentina", "Islandia", "Croacia", "Nigeria"};
int grupo_D_favoritos[] = {1, 0, 0, 0};
int grupo_D_puntos[4][8] = { { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} };


char* grupo_E[] = {"Brasil","Suiza", "Costa Rica", "Serbia"};
int grupo_E_favoritos[] = {1, 0, 0, 0};
int grupo_E_puntos[4][8] = { { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} };


char* grupo_F[] = {"Alemania", "Mexico", "Suecia", "Korea del Sur"};
int grupo_F_favoritos[] = {1, 1, 0, 0};
int grupo_F_puntos[4][8] = { { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} };


char* grupo_G[] = {"Belgica", "Panama", "Tunez", "Inglaterra"};
int grupo_G_favoritos[] = {0, 0, 0, 1};
int grupo_G_puntos[4][8] = { { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} };


char* grupo_H[] = {"Polonia", "Senegal", "Colombia", "Japón"};
int grupo_H_favoritos[] = {0, 0, 1, 0};
int grupo_H_puntos[4][8] = { { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} ,
                             { 0,0,0,0,0,0,0,0} };


void print_tabla_grupo(char* grupo_data[], int tabla[][8]){
  int i, j;
  for (i = 0; i < 4; i++){
        printf ("%s ,",grupo_data[i]);
   
        for (j = 0; j < 8; j++){
          printf ("%d ,",tabla[i][j]);
        }
        printf("\n");
  }
}




///////////////////////////////////////////////////////////////////////////
// End Info de grupos
///////////////////////////////////////////////////////////////////////////


void llenar_tabla(int tabla[][8], int equipo_1, int equipo_2, int goles_equipo_1, int goles_equipo_2)
{
  //columna 2 = PJ(Partidos Jugados)
  tabla[equipo_1][1] = tabla[equipo_1][1]+1;
  tabla[equipo_2][1] = tabla[equipo_2][1]+1;


  if(goles_equipo_1 > goles_equipo_2){
        //gano el equipo 1
        //columna 1 = Pts(Puntos)
        tabla[equipo_1][0] = tabla[equipo_1][0]+3;          
        //columna 3 = PG(Partidos Ganados)
        tabla[equipo_1][2] = tabla[equipo_1][2]+1;
        //columna 5 = PP(Partidos Perdidos)
        tabla[equipo_2][4] = tabla[equipo_2][4]+1;


  }else if (goles_equipo_1 == goles_equipo_2){
        //empataron
        tabla[equipo_1][0] = tabla[equipo_1][0]+1;          
        tabla[equipo_2][0] = tabla[equipo_2][0]+1;
        //columna 4 = PE(Partidos Empatados)
        tabla[equipo_1][3] = tabla[equipo_1][3]+1;
        tabla[equipo_2][3] = tabla[equipo_2][3]+1;          
  }else{
        //gano el equipo 2
        tabla[equipo_2][0] = tabla[equipo_2][0]+3;          
        tabla[equipo_2][2] = tabla[equipo_2][2]+1;
        //columna 5 = PP(Partidos Perdidos)
        tabla[equipo_1][4] = tabla[equipo_1][4]+1;
  }


  //columna 6 = GF(Goles a Favor)
  tabla[equipo_1][5] = tabla[equipo_1][5]+goles_equipo_1;
  tabla[equipo_2][5] = tabla[equipo_2][5]+goles_equipo_2;


  //columna 7 = GC(Goles en contra)
  tabla[equipo_1][6] = tabla[equipo_1][6]+goles_equipo_2;
  tabla[equipo_2][6] = tabla[equipo_2][6]+goles_equipo_1;


  //columna 8 = Dif(Diferencia de Gol)
  tabla[equipo_1][7] = 0; //TODO:revisar
  tabla[equipo_2][7] = 0; //TODO:revisar
}


void jugar_partido (int equipo_1, int equipo_2, int instancia, char* grupo_data[] , int grupo_puntos[][8], int grupo_favoritos[] ){
  char * nombre_equipo_1;
  char * nombre_equipo_2;
  int goles_equipo_1;
  int goles_equipo_2;

  nombre_equipo_1=grupo_data[equipo_1];
  nombre_equipo_2=grupo_data[equipo_2];
 
  /* random int entre 0 y 10 */
  int rango_gol_equipo1 = 1;
  int rango_gol_equipo2 = 1;
  int base=0;
  if(grupo_favoritos[equipo_1]){
    rango_gol_equipo1 = 7;
    base=3;
  }

  if(grupo_favoritos[equipo_2]){
    rango_gol_equipo2 = 7;
    base=3;
  }

  goles_equipo_1 = (rand() % rango_gol_equipo1)+base;
  goles_equipo_2 = (rand() % rango_gol_equipo2)+base;

  printf("Resultado %s %d -- %s %d \n", nombre_equipo_1, goles_equipo_1, nombre_equipo_2, goles_equipo_2);

  switch(instancia) {
  case 0 :
    //es instancia de grupos
    llenar_tabla(grupo_puntos, equipo_1, equipo_2, goles_equipo_1, goles_equipo_2);
    break;
  default :
    printf("Invalid instancia\n" );
  }

}

 void *jugar_grupo (int *grupo)
{

  printf("***************************************** \n");
  printf("Jugando grupo %c \n", *grupos_labels[*grupo]);
  switch(*grupo) {
 
  case 0 :

    //jugar los 6 partidos del grupo
//imprimir(grupo);

    jugar_partido(0,1, 0, grupo_A, grupo_A_puntos, grupo_A_favoritos);
    jugar_partido(0,2, 0, grupo_A, grupo_A_puntos, grupo_A_favoritos);
    jugar_partido(2,1, 0, grupo_A, grupo_A_puntos, grupo_A_favoritos);
    jugar_partido(2,3, 0, grupo_A, grupo_A_puntos, grupo_A_favoritos);
    jugar_partido(3,1, 0, grupo_A, grupo_A_puntos, grupo_A_favoritos);
    jugar_partido(3,0, 0, grupo_A, grupo_A_puntos, grupo_A_favoritos);

pthread_exit(NULL);
    break;
  case 1 :
//imprimir(grupo);
    //jugar los 6 partidos del grupo
    jugar_partido(0,1, 0, grupo_B, grupo_B_puntos, grupo_B_favoritos);
    jugar_partido(0,2, 0, grupo_B, grupo_B_puntos, grupo_B_favoritos);
    jugar_partido(2,1, 0, grupo_B, grupo_B_puntos, grupo_B_favoritos);
    jugar_partido(2,3, 0, grupo_B, grupo_B_puntos, grupo_B_favoritos);
    jugar_partido(3,1, 0, grupo_B, grupo_B_puntos, grupo_B_favoritos);
    jugar_partido(3,0, 0, grupo_B, grupo_B_puntos, grupo_B_favoritos);

pthread_exit(NULL);
    break;
  case 2 :
//imprimir(grupo);
    //jugar los 6 partidos del grupo
    jugar_partido(0,1, 0, grupo_C, grupo_C_puntos, grupo_C_favoritos);
    jugar_partido(0,2, 0, grupo_C, grupo_C_puntos, grupo_C_favoritos);
    jugar_partido(2,1, 0, grupo_C, grupo_C_puntos, grupo_C_favoritos);
    jugar_partido(2,3, 0, grupo_C, grupo_C_puntos, grupo_C_favoritos);
    jugar_partido(3,1, 0, grupo_C, grupo_C_puntos, grupo_C_favoritos);
    jugar_partido(3,0, 0, grupo_C, grupo_C_puntos, grupo_C_favoritos);

pthread_exit(NULL);
    break;
  case 3 :
//imprimir(grupo);
    //jugar los 6 partidos del grupo
    jugar_partido(0,1, 0, grupo_D, grupo_D_puntos, grupo_D_favoritos);
    jugar_partido(0,2, 0, grupo_D, grupo_D_puntos, grupo_D_favoritos);
    jugar_partido(2,1, 0, grupo_D, grupo_D_puntos, grupo_D_favoritos);
    jugar_partido(2,3, 0, grupo_D, grupo_D_puntos, grupo_D_favoritos);
    jugar_partido(3,1, 0, grupo_D, grupo_D_puntos, grupo_D_favoritos);
    jugar_partido(3,0, 0, grupo_D, grupo_D_puntos, grupo_D_favoritos);
   // print_tabla_grupo(grupo_D, grupo_D_puntos);

pthread_exit(NULL);
    break;
  case 4 :
//imprimir(grupo);
    //jugar los 6 partidos del grupo
    jugar_partido(0,1, 0, grupo_E, grupo_E_puntos, grupo_E_favoritos);
    jugar_partido(0,2, 0, grupo_E, grupo_E_puntos, grupo_E_favoritos);
    jugar_partido(2,1, 0, grupo_E, grupo_E_puntos, grupo_E_favoritos);
    jugar_partido(2,3, 0, grupo_E, grupo_E_puntos, grupo_E_favoritos);
    jugar_partido(3,1, 0, grupo_E, grupo_E_puntos, grupo_E_favoritos);
    jugar_partido(3,0, 0, grupo_E, grupo_E_puntos, grupo_E_favoritos);

pthread_exit(NULL);
    break;
  case 5 :
//imprimir(grupo);
    //jugar los 6 partidos del grupo
    jugar_partido(0,1, 0, grupo_F, grupo_F_puntos, grupo_F_favoritos);
    jugar_partido(0,2, 0, grupo_F, grupo_F_puntos, grupo_F_favoritos);
    jugar_partido(2,1, 0, grupo_F, grupo_F_puntos, grupo_F_favoritos);
    jugar_partido(2,3, 0, grupo_F, grupo_F_puntos, grupo_F_favoritos);
    jugar_partido(3,1, 0, grupo_F, grupo_F_puntos, grupo_F_favoritos);
    jugar_partido(3,0, 0, grupo_F, grupo_F_puntos, grupo_F_favoritos);

pthread_exit(NULL);
    break;
  case 6 :
//imprimir(grupo);
    //jugar los 6 partidos del grupo
    jugar_partido(0,1, 0, grupo_G, grupo_G_puntos, grupo_G_favoritos);
    jugar_partido(0,2, 0, grupo_G, grupo_G_puntos, grupo_G_favoritos);
    jugar_partido(2,1, 0, grupo_G, grupo_G_puntos, grupo_G_favoritos);
    jugar_partido(2,3, 0, grupo_G, grupo_G_puntos, grupo_G_favoritos);
    jugar_partido(3,1, 0, grupo_G, grupo_G_puntos, grupo_G_favoritos);
    jugar_partido(3,0, 0, grupo_G, grupo_G_puntos, grupo_G_favoritos);

pthread_exit(NULL);
    break;
  case 7 :
//imprimir(grupo);
    //jugar los 6 partidos del grupo
    //print_tabla_grupo(grupo_H_puntos);
    jugar_partido(0,1, 0, grupo_H, grupo_H_puntos, grupo_H_favoritos);
    jugar_partido(0,2, 0, grupo_H, grupo_H_puntos, grupo_H_favoritos);
    jugar_partido(2,1, 0, grupo_H, grupo_H_puntos, grupo_H_favoritos);
    jugar_partido(2,3, 0, grupo_H, grupo_H_puntos, grupo_H_favoritos);
    jugar_partido(3,1, 0, grupo_H, grupo_H_puntos, grupo_H_favoritos);
    jugar_partido(3,0, 0, grupo_H, grupo_H_puntos, grupo_H_favoritos);

pthread_exit(NULL);
    //print_tabla_grupo(grupo_H_puntos);
    break;

  default :
        printf("Invalid grupo\n" );
  }

}


bool coincidenciagrupo (int v[],int itera,int grupo)
{
for(int i=0;i<itera;i++)
{

     if(v[i]==grupo)
      return true;

}
return false;

}


int grupoajugar (int v[],int itera)
{
bool coincidencia=false;
int grupo=rand()%(CANTIDAD_GRUPOS);
while(coincidencia!=true){
if(coincidenciagrupo(v,itera,grupo)!=true )
{
coincidencia=true;
}

else
{
coincidencia=false;

grupo=rand()%(CANTIDAD_GRUPOS);
}

}
return grupo;
}

void jugar_grupos ()
{

//acquire()                                          //  
    ///////////////////////////////////////////////////////

sem_wait(&miSemaforo);
    pthread_mutex_lock(&mi_mutex);


  int i;
int *p;
int grupo;
int v[CANTIDAD_GRUPOS];
pthread_t p1[CANTIDAD_GRUPOS];
  for( i=0; i < CANTIDAD_GRUPOS; i++ ){

grupo=grupoajugar(v,i);
v[i]=grupo;
pthread_create(&p1[i],NULL,(void *)jugar_grupo,&grupo);
pthread_join(p1[i],NULL);


  } 
  
 //release()                                          //  
    ///////////////////////////////////////////////////////
    pthread_mutex_unlock(&mi_mutex);
sem_post(&miSemaforo2);
pthread_exit(NULL);
}




char* winer_octavos[] = {"Wp49", "Wp50", "Wp51", "Wp52", "Wp53", "Wp54", "Wp55", "Wp56"};




void *jugar_partido_winners(void*p){
struct datpartido *datp=(struct datpartido*) p;
 
 printf("Jugando partido entre %s y %s \n", datp->nombre_equipo_1, datp->nombre_equipo_2);
  int goles_equipo_1;
  int goles_equipo_2;
  /* random int entre 0 y 10 */
  goles_equipo_1 = rand() % 10;
  goles_equipo_2 = rand() % 10;
  if(goles_equipo_1 > goles_equipo_2){
        datp->data_winners[datp->index_partido]=datp->nombre_equipo_1;
  }else{
        datp->data_winners[datp->index_partido]=datp->nombre_equipo_2;
  }

pthread_exit(NULL);
}


int obtener_primero(int grupo_puntos[][8]){
  int i;
  int puntaje_maximo=-1;
  int index_maximo=-1;
  for (i = 0; i < 4; i++){
        if(grupo_puntos[i][0]>=puntaje_maximo){
          index_maximo=i;
          puntaje_maximo=grupo_puntos[i][0];
        }
  }
  return index_maximo;
}


int obtener_segundo(int grupo_puntos[][8]){
  int i;
  int puntaje_maximo=-1;
  int index_segundo=-1;


  int index_primero=obtener_primero(grupo_puntos);
  for (i = 0; i < 4; i++){
        if(grupo_puntos[i][0]>=puntaje_maximo && i!=index_primero){
          index_segundo=i;
          puntaje_maximo=grupo_puntos[i][0];
        }
  }
  return index_segundo;
}


void* jugar_octavos ()
{

sem_wait(&miSemaforo2);
    pthread_mutex_lock(&mi_mutex);
 srand(time(NULL));
   



  int index_1;
  char * nombre_equipo_1;
  int index_2;
  char * nombre_equipo_2;


  printf("***************************************** \n");
  printf("Jugando octavos \n");
pthread_t t1,t2,t3,t4,t5,t6,t7,t8;

  //partido 49: A1 vs B2
  index_1=obtener_primero(grupo_A_puntos);
  nombre_equipo_1=grupo_A[index_1];
  index_2=obtener_segundo(grupo_B_puntos);
  nombre_equipo_2=grupo_B[index_2];
struct datpartido datp={
		nombre_equipo_1,
		nombre_equipo_2,
		0,
		winer_octavos
		};

pthread_create(&t1,NULL,(void *)jugar_partido_winners,(void*)&datp);
 

  //partido 50: C1 vs D2


  index_1=obtener_primero(grupo_C_puntos);
  nombre_equipo_1=grupo_C[index_1];
  index_2=obtener_segundo(grupo_D_puntos);
  nombre_equipo_2=grupo_D[index_2];


 struct datpartido datp2={
		nombre_equipo_1,
		nombre_equipo_2,
		1,
		winer_octavos
		};

pthread_create(&t2,NULL,(void *)jugar_partido_winners,(void*)&datp2);
 


  //partido 51: B1 vs A2
  index_1=obtener_primero(grupo_B_puntos);
  nombre_equipo_1=grupo_B[index_1];
  index_2=obtener_segundo(grupo_A_puntos);
  nombre_equipo_2=grupo_A[index_2];



struct datpartido datp3={
		nombre_equipo_1,
		nombre_equipo_2,
		2,
		winer_octavos
		};

pthread_create(&t3,NULL,(void *)jugar_partido_winners,(void*)&datp3);


  //partido 52: D1 vs C2
  index_1=obtener_primero(grupo_D_puntos);
  nombre_equipo_1=grupo_D[index_1];
  index_2=obtener_segundo(grupo_C_puntos);
  nombre_equipo_2=grupo_C[index_2];


 struct datpartido datp4={
		nombre_equipo_1,
		nombre_equipo_2,
		3,
		winer_octavos
		};

pthread_create(&t4,NULL,(void *)jugar_partido_winners,(void*)&datp4);



  //partido 53: E1 vs F2
  index_1=obtener_primero(grupo_E_puntos);
  nombre_equipo_1=grupo_E[index_1];
  index_2=obtener_segundo(grupo_F_puntos);
  nombre_equipo_2=grupo_F[index_2];


  struct datpartido datp5={
		nombre_equipo_1,
		nombre_equipo_2,
		4,
		winer_octavos
		};

pthread_create(&t5,NULL,(void *)jugar_partido_winners,(void*)&datp5);




  //partido 54: G1 vs H2
  index_1=obtener_primero(grupo_G_puntos);
  nombre_equipo_1=grupo_G[index_1];
  index_2=obtener_segundo(grupo_H_puntos);
  nombre_equipo_2=grupo_H[index_2];




 struct datpartido datp6={
		nombre_equipo_1,
		nombre_equipo_2,
		5,
		winer_octavos
		};

pthread_create(&t6,NULL,(void *)jugar_partido_winners,(void*)&datp6);



  //partido 55: F1 vs E2
  index_1=obtener_primero(grupo_F_puntos);
  nombre_equipo_1=grupo_F[index_1];
  index_2=obtener_segundo(grupo_E_puntos);
  nombre_equipo_2=grupo_E[index_2];



 struct datpartido datp7={
		nombre_equipo_1,
		nombre_equipo_2,
		6,
		winer_octavos
		};

pthread_create(&t7,NULL,(void *)jugar_partido_winners,(void*)&datp7);


  //partido 56: H1 vs G2
  index_1=obtener_primero(grupo_H_puntos);
  nombre_equipo_1=grupo_H[index_1];
  index_2=obtener_segundo(grupo_G_puntos);
  nombre_equipo_2=grupo_G[index_2];



 struct datpartido datp8={
		nombre_equipo_1,
		nombre_equipo_2,
		7,
		winer_octavos
		};



pthread_create(&t8,NULL,(void *)jugar_partido_winners,(void*)&datp8);



pthread_join(t1,NULL);
pthread_join(t2,NULL);
pthread_join(t3,NULL);
pthread_join(t4,NULL);
pthread_join(t5,NULL);
pthread_join(t6,NULL);
pthread_join(t7,NULL);
pthread_join(t8,NULL);


    pthread_mutex_unlock(&mi_mutex);


sem_post(&miSemaforo3);
pthread_exit(NULL);

}


char* winer_cuartos[] = {"Wp57", "Wp58", "Wp59", "Wp60"};


void* jugar_cuartos ()
{

sem_wait(&miSemaforo3);
 pthread_mutex_lock(&mi_mutex);
 srand(time(NULL));
   
  printf("***************************************** \n");
  printf("Jugando cuartos \n");
  int index_1;
  char * nombre_equipo_1;
  int index_2;
  char * nombre_equipo_2;


  //partido 57: Wp49 vs Wp50
pthread_t t[4];


 struct datpartido datp1={
		winer_octavos[0],
		winer_octavos[1],
		0,
		winer_cuartos
		};

pthread_create(&t[0],NULL,(void *)jugar_partido_winners,(void*)&datp1);


pthread_join(t[0],NULL);
    
  //partido 58: Wp53 vs Wp54



 struct datpartido datp2={
		winer_octavos[4],
		winer_octavos[5],
		1,
		winer_cuartos
		};

pthread_create(&t[1],NULL,(void *)jugar_partido_winners,(void*)&datp2);

  //partido 59: Wp51 vs Wp52



 struct datpartido datp3={
		winer_octavos[2],
		winer_octavos[3],
		2,
		winer_cuartos
		};

pthread_create(&t[2],NULL,(void *)jugar_partido_winners,(void*)&datp3);


  //partido 60: Wp55 vs Wp56



 struct datpartido datp4={
		winer_octavos[6],
		winer_octavos[7],
		3,
		winer_cuartos
		};

pthread_create(&t[3],NULL,(void *)jugar_partido_winners,(void*)&datp4);


pthread_join(t[0],NULL);
pthread_join(t[1],NULL);

pthread_join(t[2],NULL);
pthread_join(t[3],NULL);

    pthread_mutex_unlock(&mi_mutex);
sem_post(&miSemaforo4);

pthread_exit(NULL);
}




char* winer_semis[] = {"Wp61", "Wp62"};
char* champion[] = {"Wfinal"};
void *jugar_semis ()
{
sem_wait(&miSemaforo4);
 pthread_mutex_lock(&mi_mutex);
 srand(time(NULL));
   
  printf("***************************************** \n");
  printf("Jugando semis \n");
  //partido 61: Wp57 vs Wp58
 pthread_t t[2];
struct datpartido datp1={
		winer_cuartos[0],
		winer_cuartos[1],
		0,
		winer_semis
		};
 
  //partido 62: Wp59 vs Wp60


pthread_create(&t[0],NULL,(void *)jugar_partido_winners,(void*)&datp1);

struct datpartido datp2={
		winer_cuartos[2],
		winer_cuartos[3],
		1,
		winer_semis
		};
 
pthread_create(&t[1],NULL,(void *)jugar_partido_winners,(void*)&datp2);


pthread_join(t[0],NULL);
pthread_join(t[1],NULL);
    pthread_mutex_unlock(&mi_mutex);


sem_post(&miSemaforo5);
pthread_exit(NULL);
}


void* jugar_final ()
{
sem_wait(&miSemaforo5);
 pthread_mutex_lock(&mi_mutex);
 srand(time(NULL));
   
  printf("***************************************** \n");
 
  printf("Jugando final \n");
  //final: Wp91 vs Wp62
pthread_t t1;

struct datpartido datp1={
		winer_semis[0],
		winer_semis[1],
		0,
		champion
		};

pthread_create(&t1,NULL,(void *)jugar_partido_winners,(void*)&datp1);

pthread_join(t1,NULL);
  printf("***************************************** \n");
  printf("El nuevo campeon del mundo es %s \n", champion[0]);
  printf("***************************************** \n");
    pthread_mutex_unlock(&mi_mutex);

pthread_exit(NULL);
}


int main ()
{
  //////////////////////////////////////////////////////////////////////
  // start mundial
  //////////////////////////////////////////////////////////////////////
  pthread_mutex_lock(&mi_mutex);

 srand(time(NULL));


    
   pthread_mutex_init ( &mi_mutex, NULL);
pthread_t p1;
pthread_t p2;

pthread_t p3;
pthread_t p4;
pthread_t p5;
int rc;
 
 sem_init(&miSemaforo, //se inicializa el semaforo pasado por referencia
              0,1);//valor inicial para el semaforo
 sem_init(&miSemaforo2, //se inicializa el semaforo pasado por referencia
              0,0);//valor inicial para el semaforo
 sem_init(&miSemaforo3, //se inicializa el semaforo pasado por referencia
              0,0);//valor inicial para el semaforo
	
 sem_init(&miSemaforo4, //se inicializa el semaforo pasado por referencia
              0,0);//valor inicial para el semaforo
 sem_init(&miSemaforo5, //se inicializa el semaforo pasado por referencia
              0,0);//valor inicial para el semaforo
	
rc=pthread_create(&p1,NULL,(void *)jugar_grupos,NULL);

rc=pthread_create(&p2,NULL,(void *)jugar_octavos,NULL);//
rc=pthread_create(&p3,NULL,(void *)jugar_cuartos,NULL);//
rc=pthread_create(&p4,NULL,(void *)jugar_semis,NULL);


rc=	pthread_create(&p5,NULL,(void *)jugar_final,NULL);

if (rc){
       printf("Error:unable to create thread, %d \n", rc);
       exit(-1);
     }
	pthread_join(p1,NULL);

	pthread_join(p2,NULL);
	pthread_join(p3,NULL);

	pthread_join(p4,NULL);

	pthread_join(p5,NULL);



 pthread_exit(NULL);
    pthread_mutex_destroy(&mi_mutex);
  /////////////////////////////////////////////////////////////////////
  // end mundial
  //////////////////////////////////////////////////////////////////////
}


//Para compilar:   gcc simulador.c -o simulador
//Para ejecutar:   ./simulador